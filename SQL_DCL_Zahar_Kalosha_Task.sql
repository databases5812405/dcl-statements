-- 1)

CREATE USER rentaluser WITH PASSWORD 'rentalpassword';

GRANT CONNECT ON DATABASE dvdrental1 TO rentaluser;

-- 2)

GRANT SELECT ON TABLE customer TO rentaluser;

SET ROLE rentaluser;

SELECT * FROM customer;

RESET ROLE;

-- 3)

CREATE GROUP rental;

ALTER GROUP rental ADD USER rentaluser;

-- 4)

GRANT INSERT, SELECT, UPDATE ON TABLE rental TO rental;

GRANT USAGE, SELECT ON SEQUENCE rental_rental_id_seq TO rental;

SET ROLE rentaluser;

INSERT INTO rental (rental_date, inventory_id, customer_id, return_date, staff_id, last_update)
VALUES (CURRENT_DATE , 123, 456, CURRENT_DATE , 5, NOW());

UPDATE rental SET return_date = CURRENT_DATE  WHERE rental_id = 1;

RESET ROLE;

-- 5)

REVOKE INSERT ON TABLE rental FROM rental;

SET ROLE rentaluser;

INSERT INTO rental(rental_date,inventory_id , customer_id, return_date,staff_id ,last_update)
VALUES(CURRENT_DATE , 234, 567, CURRENT_DATE, 890, NOW());

RESET ROLE;

-- 6) TODO